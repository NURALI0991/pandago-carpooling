package it.polito.carpooling

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import org.json.JSONObject


/**
 * A fragment representing a list of Items.
 */
class TripListFragment : Fragment() {

    private var columnCount = 1


    private var adapter: MyTripRecyclerViewAdapter? = null

    private var db = FirebaseFirestore.getInstance()
    private val tripsCollection = db.collection("trips")

    private val tripsArray = arrayListOf<Trip>()

    private lateinit var recView: RecyclerView
    private lateinit var empView: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)

        val view = inflater.inflate(R.layout.fragment_trip_list_list, container, false)
        val test = activity?.findViewById<FloatingActionButton>(R.id.fab)
        if (test != null) {
            test.visibility = View.VISIBLE
        }
        recView = view.findViewById<RecyclerView>(R.id.list)
        empView = view.findViewById<TextView>(R.id.empty_view)

        tripsArray.clear()

        readSharedPreferenceTrips()

        recView.layoutManager
        adapter = MyTripRecyclerViewAdapter(tripsArray)
        recView.adapter = adapter

        return view
    }

    private fun showNoTripsLayout(recView: RecyclerView, empView: TextView) {
        recView.visibility = View.GONE
        empView.visibility = View.VISIBLE
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        val item = menu.findItem(R.id.action_settings)
        val edit = menu.findItem(R.id.edit)
        val save = menu.findItem(R.id.save)
        //item.isVisible = false
        edit.isVisible = false
        save.isVisible = false

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                (activity as MainActivity?)?.userLogOut()
            }
        }
        return true
    }


    private fun readSharedPreferenceTrips() {

        val userEmail = FirebaseAuth.getInstance().currentUser?.email

        if (userEmail != null) {
            tripsCollection.whereEqualTo("userEmail", userEmail)
                .get()
                .addOnSuccessListener { documents ->
                    if (documents.size() == 0)
                        showNoTripsLayout(recView, empView)

                    for (document in documents) {
                        Log.d("TAG", "${document.id} => ${document.data}")
                        val trip = Trip(
                            document.id,
                            document["departureLocation"].toString(),
                            document["arrivalLocation"].toString(),
                            document["departureDate"] as Long,
                            (document["duration"] as Long).toInt(),
                            (document["availableSeats"] as Long).toInt(),
                            (document["price"] as Long).toInt(),
                            document["description"].toString()
                        )
                        trip.userEmail = document["userEmail"].toString()

                        if (document["completed"] == null || document["completed"] == false) {
                            tripsArray.add(trip)
                            adapter?.notifyDataSetChanged()
                        }

                    }
                    if (tripsArray.isEmpty())
                        showNoTripsLayout(recView, empView)

                }
                .addOnFailureListener { exception ->
                    Log.w("TAG", "Error getting documents: ", exception)
                    showNoTripsLayout(recView, empView)
                }
        }

    }

    fun onItemClick(view: View?, position: Int) {
        Toast.makeText(
            activity,
            "You clicked " + adapter?.getItem(position).toString() + " on row number " + position,
            Toast.LENGTH_SHORT
        ).show()
    }


    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            TripListFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}