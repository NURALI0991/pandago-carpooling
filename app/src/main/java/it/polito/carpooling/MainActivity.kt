package it.polito.carpooling

import android.content.Intent
import android.graphics.*
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException

import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.core.app.ActivityCompat
import androidx.core.os.bundleOf
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import org.osmdroid.config.Configuration
import org.osmdroid.views.MapView
import java.util.ArrayList


private const val FILE_NAME = "photo.jpg"

class MainActivity : AppCompatActivity() {
    private lateinit var appBarConfiguration: AppBarConfiguration


    private val user = User()

    private lateinit var header: View

    private lateinit var firebaseAuth: FirebaseAuth

    private val REQUEST_PERMISSIONS_REQUEST_CODE = 1;
    lateinit var map: CustomMapView;

    var db = FirebaseFirestore.getInstance()
    val usersProfile = db.collection("users")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            view.visibility = View.GONE
            findNavController(R.id.nav_host_fragment).navigate(R.id.action_nav_home_to_nav_editTrip)
        }

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.nav_editTrip,
                R.id.nav_showTrip,
                R.id.nav_showProfile,
                R.id.nav_editProfile,
                R.id.nav_IntermediateStop,
                R.id.nav_othersTripList,
                R.id.nav_boughtTripList,
                R.id.nav_interestedTripList,
                R.id.nav_rateAcceptedUsers
            ), drawerLayout
        )

        val toggle: ActionBarDrawerToggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_open
        )
        drawerLayout.addDrawerListener(toggle)

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


        header = navView.getHeaderView(0)

        firebaseAuth = FirebaseAuth.getInstance()
        updateThumbProfile()
    }

    fun updateThumbProfile() {
        val userEmail = FirebaseAuth.getInstance().currentUser?.email
        val res = arrayListOf<String>()
        if (userEmail != null) {
            usersProfile.document(userEmail).get()
                .addOnSuccessListener { document ->
                    if (document != null && document["fullName"] != null) {
                        res.add(document["fullName"].toString())
                        res.add(document["nickName"].toString())
                        res.add(document["email"].toString())
                        res.add(document["location"].toString())
                        user.fullName = res[0]
                        user.nickName = res[1]
                        user.email = res[2]
                        user.location = res[3]

                    }
                    header.findViewById<TextView>(R.id.thumbFullName).text = user.fullName
                    header.findViewById<TextView>(R.id.thumbEmail).text = user.email
                }
                .addOnFailureListener { exception ->
                }
        }

        val b = loadImage(filesDir.path + "/../app_images")
        if (b != null) {
            header.findViewById<ImageView>(R.id.thumbProfileImg)
                .setImageBitmap(Bitmap.createScaledBitmap(b, 220, 220, false));
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        menuInflater.inflate(R.menu.main, menu)

        return true
    }

    fun userLogOut() {
        firebaseAuth.signOut()
        checkUser()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }


    private fun loadImage(path: String): Bitmap? {

        var b: Bitmap? = null
        try {
            val f = File(path, FILE_NAME)
            b = BitmapFactory.decodeStream(FileInputStream(f))
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        return b

    }


    private fun checkUser() {
        val firebaseUser = firebaseAuth.currentUser
        if (firebaseUser == null) {
            startActivity(Intent(this, SignActivity::class.java))
            finish()
        } else {
            //TODO add logout button firebaseAuth.singOut()
        }
    }

    override fun onResume() {
        super.onResume();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        if(this::map.isInitialized)
            map.onResume(); //needed for compass, my location overlays, v6.0.0 and up
    }

    override fun onPause() {
        super.onPause();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        if(this::map.isInitialized)
            map.onPause();  //needed for compass, my location overlays, v6.0.0 and up
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val permissionsToRequest = ArrayList<String>();
        var i = 0;
        while (i < grantResults.size) {
            permissionsToRequest.add(permissions[i]);
            i++;
        }
        if (permissionsToRequest.size > 0) {
            ActivityCompat.requestPermissions(
                this,
                permissionsToRequest.toTypedArray(),
                REQUEST_PERMISSIONS_REQUEST_CODE
            );
        }
    }

}