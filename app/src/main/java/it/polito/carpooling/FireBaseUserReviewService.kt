package it.polito.carpooling

import android.annotation.SuppressLint
import android.provider.ContactsContract
import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import it.polito.carpooling.UserReview.Companion.toUserReview
import kotlinx.coroutines.tasks.await
import java.lang.Exception

object FireBaseUserReviewService {
    private const val TAG = "FireBase"

    suspend fun getUserReviewData(reviewId: String): UserReview? {
        val db = FirebaseFirestore.getInstance()
        return try {
            db.collection("userReviews").document(reviewId).get().await().toUserReview()
        } catch (e: Exception) {
            Log.e(TAG, "Error getting user review", e)
            null
        }
    }

    suspend fun getUserReviewOfUser(userEmail: String): List<UserReview> {
        val db = FirebaseFirestore.getInstance()
        return try {
            db.collection("userReviews").whereEqualTo("userEmail", userEmail).get().await()
                .documents.mapNotNull { it.toUserReview() }
        } catch (e: Exception) {
            emptyList<UserReview>()
        }
    }

    fun rateAUser(userReview: UserReview) {
        val db = FirebaseFirestore.getInstance()
        db.collection("userReviews").add(userReview)

    }

}