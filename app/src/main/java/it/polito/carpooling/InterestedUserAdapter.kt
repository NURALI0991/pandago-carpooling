package it.polito.carpooling

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import java.util.*

class InterestedUserAdapter(private val values: ArrayList<User>, private val fragment: Fragment) :
    RecyclerView.Adapter<InterestedUserAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.interested_user_card, parent, false)


        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]


        holder.email.text = item.email

        if (fragment is TripDetailsFragment) {
            holder.switch.visibility = View.GONE
            val tmpDetailsFragment = fragment as TripDetailsFragment
            tmpDetailsFragment.checkIfUserIsAccepted(item.email,holder.tick)
        } else {
            val tmpEditFragment = fragment as TripEditFragment
            tmpEditFragment.checkIfUserIsAccepted(item.email, holder.switch)
            holder.tick.visibility = View.GONE
        }


        holder.card.setOnClickListener { view ->
            val bundle = bundleOf("userEmail" to item.email)
            if (fragment is TripDetailsFragment)
                view.findNavController().navigate(
                    R.id.action_nav_showTrip_to_nav_showProfile,
                    bundle
                )
            else {
                view.findNavController().navigate(
                    R.id.action_nav_editTrip_to_nav_showProfile,
                    bundle
                )
            }

        }


    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val email: TextView = view.findViewById(R.id.emailInterested)
        val card: MaterialCardView = view.findViewById(R.id.cardInterested)
        val switch: SwitchCompat = view.findViewById(R.id.acceptUser)
        val tick:ImageView = view.findViewById(R.id.tickView)

    }

}