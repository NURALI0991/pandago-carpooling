package it.polito.carpooling

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import it.polito.carpooling.dummy.DummyContent.DummyItem

/**
 * [RecyclerView.Adapter] that can display a [DummyItem].
 * TODO: Replace the implementation with code for your data type.
 */
class MyTripRecyclerViewAdapter(
    private val values: ArrayList<Trip>
) : RecyclerView.Adapter<MyTripRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_trip_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.departure.text = item.departureLocation
        holder.arrival.text = item.arrivalLocation
        holder.nAvailableSits.text = item.availableSeats.toString()
        holder.editButton.setOnClickListener { view ->
            val bundle = bundleOf("tripId" to item.id)
            view.findNavController().navigate(R.id.action_nav_home_to_nav_editTrip, bundle)
        }
        holder.contentView.setOnClickListener { view ->
            val bundle = bundleOf("tripId" to item.id)
            bundle.putInt("fromList", 1  )
            view.findNavController().navigate(R.id.action_nav_home_to_nav_showTrip, bundle)
        }
    }

    override fun getItemCount(): Int = values.size

    fun getItem(position: Int): String {
        return values[position].description
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val editButton: Button = view.findViewById(R.id.editRecycle)
        val contentView: LinearLayout = view.findViewById(R.id.restOfTheCard)
        val departure = view.findViewById<TextView>(R.id.cardDeparture)
        val arrival = view.findViewById<TextView>(R.id.cardArrival)
        val nAvailableSits = view.findViewById<TextView>(R.id.cardNAvailableSits)
        override fun toString(): String {
            return "TEST"
        }
    }
}