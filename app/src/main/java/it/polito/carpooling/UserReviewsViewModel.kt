package it.polito.carpooling

import android.provider.ContactsContract
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class UserReviewsViewModel(val userEmail: String) : ViewModel() {

    private val _reviews = MutableLiveData<List<UserReview>>()
    val reviews: LiveData<List<UserReview>> = _reviews

    init {
        viewModelScope.launch {
            _reviews.value = FireBaseUserReviewService.getUserReviewOfUser(userEmail)
        }
    }
}