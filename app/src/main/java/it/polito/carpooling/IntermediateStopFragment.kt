package it.polito.carpooling

import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.Marker.OnMarkerClickListener
import org.osmdroid.views.overlay.Overlay
import org.osmdroid.views.overlay.Polyline
import java.util.*
import kotlin.collections.ArrayList


/**
 * A fragment representing a list of Items.
 */
class IntermediateStopFragment : Fragment() {

    private var columnCount = 1

    var adapter: MyIntermediateStopRecyclerViewAdapter? = null
    var tripId = "new";

    private var list = arrayListOf<IntermediateStop>()
    private var intermediateMapPointList = arrayListOf<GeoPoint>()

    private lateinit var locationEdit: EditText
    private lateinit var saveButton: Button
    private lateinit var editDate: DatePicker
    private lateinit var editTime: TimePicker

    private var db = FirebaseFirestore.getInstance()
    private val tripsCollection = db.collection("trips")

    private lateinit var map: CustomMapView
    private lateinit var polyline: Polyline

    private lateinit var buttonAddIntermediateStop: Button
    private lateinit var buttonRemoveIntermediateStop: Button

    private var editMapMod = EditMapMode.NONE
    private lateinit var buttonGoUp: Button


    private var departureGeo: GeoPoint? = null
    private var arrivalGeo: GeoPoint? = null

    private lateinit var recView: RecyclerView
    private lateinit var empView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        var view = inflater.inflate(R.layout.fragment_intermediate_stop_list, container, false)

        val orientation = this.resources.configuration.orientation


        if (orientation != Configuration.ORIENTATION_PORTRAIT) {
            view = inflater.inflate(R.layout.fragment_intermediate_stop_list_land, container, false)
        }

        recView = view.findViewById<RecyclerView>(R.id.listIntermediateStop)
        empView = view.findViewById<TextView>(R.id.empty_view)

        locationEdit = view.findViewById(R.id.editLocationIntermediate);
        saveButton = view.findViewById(R.id.buttonSaveIntermediate);
        saveButton.setOnClickListener {
            addIntermediateStop()
        }

        editDate = view.findViewById(R.id.DateIntermediateStopShow)
        editDate.minDate = System.currentTimeMillis() - 1000
        editTime = view.findViewById(R.id.TimeIntermediateStopShow)
        // Set the adapter
        recView.layoutManager



        hideFloatingButton()

        val id = arguments?.getString("tripId")
        val owner = arguments?.getBoolean("fromList")
        if (id != null) {
            tripId = id;
            list.clear()
            readSharedPreferenceTrips()
        }

        mapInit(view)
        buttonMapInit(view)

        adapter = if (owner != true) {
            hideAllEditStuff()
            MyIntermediateStopRecyclerViewAdapter(list, this, false);
        } else {
            MyIntermediateStopRecyclerViewAdapter(list, this, true);
        }
        recView.adapter = adapter




        return view
    }


    private fun hideAllEditStuff() {
        locationEdit.visibility = View.GONE
        saveButton.visibility = View.GONE
        editDate.visibility = View.GONE
        editTime.visibility = View.GONE
        buttonRemoveIntermediateStop.visibility = View.GONE
        buttonAddIntermediateStop.visibility = View.GONE
    }

    private fun hideFloatingButton() {
        val test = activity?.findViewById<FloatingActionButton>(R.id.fab)
        if (test != null) {
            test.visibility = View.GONE
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hideFloatingButton()
    }

    private fun writeSharedPreferenceIntermediateStop(intermediateStop: IntermediateStop) {


        tripsCollection.document(tripId)
            .update("intermediateStops", FieldValue.arrayUnion(intermediateStop))


    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        val item = menu.findItem(R.id.action_settings)
        val edit = menu.findItem(R.id.edit)
        val save = menu.findItem(R.id.save)
        item.isVisible = false
        edit.isVisible = false
        save.isVisible = false

    }

    private fun addIntermediateStop() {

        if (list.isEmpty())
            showIntermediate()

        val cal = Calendar.getInstance()
        cal[Calendar.DAY_OF_MONTH] = editDate.dayOfMonth
        cal[Calendar.MONTH] = editDate.month
        cal[Calendar.YEAR] = editDate.year
        cal[Calendar.HOUR] = editTime.currentHour
        cal[Calendar.MINUTE] = editTime.currentMinute

        val intermediateStop =
            IntermediateStop(list.size, locationEdit.text.toString(), cal.timeInMillis)
        list.add(intermediateStop)
        adapter?.notifyDataSetChanged()
        writeSharedPreferenceIntermediateStop(intermediateStop)
    }

    private fun readSharedPreferenceTrips() {
        tripsCollection.document(tripId).get().addOnSuccessListener { document ->
            if (document["intermediateStops"] != null) {
                val listTmp = document["intermediateStops"] as ArrayList<HashMap<String, Any>>
                if (listTmp.isEmpty())
                    showNoIntermediateLayout()
                for (e in listTmp) {
                    list.add(
                        IntermediateStop(
                            (e["id"] as Long).toInt(),
                            e["location"].toString(),
                            e["estimatedTime"] as Long
                        )
                    )
                    adapter?.notifyDataSetChanged()
                }

            } else {
                showNoIntermediateLayout()
            }

            if (document["arrivalGeo"] != null && document["departureGeo"] != null) {
                val arrivalMap = document["arrivalGeo"] as Map<*, *>
                val departureMap = document["departureGeo"] as Map<*, *>
                val geoPoint1 =
                    GeoPoint(arrivalMap["latitude"] as Double, arrivalMap["longitude"] as Double)
                val geoPoint2 = GeoPoint(
                    departureMap["latitude"] as Double,
                    departureMap["longitude"] as Double
                )
                if (geoPoint1.latitude != 0.0 && geoPoint2.latitude != 0.0) {
                    arrivalGeo = geoPoint1
                    departureGeo = geoPoint2
                    createMarkersOnMap(map, geoPoint1, geoPoint2)
                } else {
                    buttonRemoveIntermediateStop.visibility = View.GONE
                    buttonAddIntermediateStop.visibility = View.GONE
                }

                if (document["intermediateGeo"] != null) {
                    val list = document["intermediateGeo"] as ArrayList<Map<*, *>>
                    for (e in list) {
                        val geoInt =
                            GeoPoint(e["latitude"] as Double, e["longitude"] as Double)
                        intermediateMapPointList.add(geoInt)
                        createIntermediateMarker(map, geoInt)
                    }

                }
                updatePolyLine()
            }


        }


    }

    private fun mapInit(view: View) {
        (activity as MainActivity).map = view.findViewById(R.id.mapShow) as CustomMapView
        map = (activity as MainActivity).map
        map.setTileSource(TileSourceFactory.MAPNIK);
        val mapController = (activity as MainActivity).map.controller
        mapController.setZoom(7.0)
        val startPoint = GeoPoint(41.8719, 12.5674);
        mapController.setCenter(startPoint);
        polyline = Polyline()
        polyline.color = Color.RED
        polyline.width = 10F
        map.overlays.add(polyline);

        map.overlays.add(object : Overlay() {

            override fun onSingleTapConfirmed(
                e: MotionEvent,
                mapView: MapView
            ): Boolean {
                val projection =
                    mapView.projection
                val geoPoint = projection.fromPixels(
                    e.x.toInt(),
                    e.y.toInt()
                )


                when (editMapMod) {
                    EditMapMode.NONE -> {

                    }
                    EditMapMode.ADD_INTERMEDIATE -> {
                        if (intermediateMapPointList.size < list.size) {
                            createIntermediateMarker(
                                mapView,
                                GeoPoint(geoPoint.latitude, geoPoint.longitude)
                            )
                            updatePolyLine()
                        }
                    }
                    else -> {
                    }
                }
                return true
            }
        })

    }


    private fun buttonMapInit(view: View) {


        buttonAddIntermediateStop = view.findViewById(R.id.buttonAddIntermediate)
        buttonAddIntermediateStop.setOnClickListener {
            when (editMapMod) {
                EditMapMode.NONE -> {
                    buttonAddIntermediateStop.text = "Confirm Intermediates"
                    editMapMod = EditMapMode.ADD_INTERMEDIATE
                    buttonRemoveIntermediateStop.visibility = View.GONE
                }
                EditMapMode.ADD_INTERMEDIATE -> {
                    //STORE the intermediate GEO POINT
                    editMapMod = EditMapMode.NONE
                    buttonAddIntermediateStop.text = "Add"
                    buttonRemoveIntermediateStop.visibility = View.VISIBLE
                    updateIntermediateGeo()
                    //Clean
                }
                else -> {

                }
            }
        }


        buttonRemoveIntermediateStop = view.findViewById(R.id.buttonRemoveIntermediate)
        buttonRemoveIntermediateStop.setOnClickListener {
            when (editMapMod) {
                EditMapMode.NONE -> {
                    buttonRemoveIntermediateStop.text = "Confirm intermediates"
                    editMapMod = EditMapMode.REMOVE_INTERMEDIATE
                    buttonAddIntermediateStop.visibility = View.GONE
                }
                EditMapMode.REMOVE_INTERMEDIATE -> {
                    //Store GEO POINTS
                    buttonRemoveIntermediateStop.text = "Remove"
                    editMapMod = EditMapMode.NONE
                    buttonAddIntermediateStop.visibility = View.VISIBLE
                    updateIntermediateGeo()
                    //CLEAN
                }
                else -> {

                }
            }
        }

        buttonGoUp = view.findViewById(R.id.UpButton)
        buttonGoUp.setOnClickListener { it ->
            view.findViewById<ScrollView>(R.id.scrollView).fullScroll(ScrollView.FOCUS_UP);
        }
    }


    fun deleteSharedPreferenceIntermediateStop(intermediateStop: IntermediateStop) {
        tripsCollection.document(tripId)
            .update("intermediateStops", FieldValue.arrayRemove(intermediateStop))

    }

    private var markerId = 0
    private fun createIntermediateMarker(mapView: MapView, geoPoint: GeoPoint) {
        val intermediateMarker = Marker(mapView)
        intermediateMarker.id = markerId.toString()
        intermediateMarker.icon =
            context?.let { ContextCompat.getDrawable(it, R.drawable.ic_intermediate_marker) }
        intermediateMarker.position = GeoPoint(geoPoint.latitude, geoPoint.longitude)
        intermediateMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
        intermediateMarker.title = "Intermediate"
        mapView.overlays.add(intermediateMarker)

        intermediateMarker.setOnMarkerClickListener { item, arg1 ->
            when (editMapMod) {
                EditMapMode.NONE -> {

                }
                EditMapMode.REMOVE_INTERMEDIATE -> {
                    removeMarker(mapView, item.id)
                    updateIntermediateGeo()
                }
                else -> {

                }
            }
            true
        }
        geoPoint.altitude = markerId.toDouble()
        intermediateMapPointList.add(geoPoint)
        markerId++
    }


    private fun createMarkersOnMap(mapView: MapView, geoPoint1: GeoPoint, geoPoint2: GeoPoint) {
        val arrivalMarker = Marker(mapView)
        arrivalMarker.id = "Arrival"
        arrivalMarker.position = GeoPoint(geoPoint1.latitude, geoPoint1.longitude);
        arrivalMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
        arrivalMarker.icon =
            context?.let { ContextCompat.getDrawable(it, R.drawable.ic_destination_marker) }
        mapView.overlays.add(arrivalMarker)
        arrivalMarker.title = "Arrival"
        val departureMarker = Marker(mapView)
        departureMarker.id = "Departure"
        departureMarker.position =
            GeoPoint(geoPoint2.latitude, geoPoint2.longitude)
        departureMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
        departureMarker.icon = context?.let { ContextCompat.getDrawable(it, R.drawable.person) }
        departureMarker.title = "Departure"



        mapView.overlays.add(departureMarker)

    }


    private fun removeMarker(map: MapView, id: String) {
        map.overlays.forEach {
            if (it is Marker && it.id == id) {
                map.overlays.remove(it)
            }
        }
        intermediateMapPointList =
            intermediateMapPointList.filter { it.altitude != id.toDouble() } as ArrayList<GeoPoint>
        updatePolyLine()
    }

    private fun updatePolyLine() {
        if (departureGeo != null && arrivalGeo != null) {
            val pathPoints = arrayListOf<GeoPoint>()
            pathPoints.add(departureGeo!!)
            pathPoints.addAll(intermediateMapPointList)
            pathPoints.add(arrivalGeo!!)
            polyline.setPoints(pathPoints)
        }


    }


    private fun updateIntermediateGeo() {
        tripsCollection.document(tripId)
            .update("intermediateGeo", intermediateMapPointList)
    }

    fun showNoIntermediateLayout() {
        recView.visibility = View.GONE
        empView.visibility = View.VISIBLE
    }

    private fun showIntermediate() {
        recView.visibility = View.VISIBLE
        empView.visibility = View.GONE
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            IntermediateStopFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}