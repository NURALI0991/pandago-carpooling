package it.polito.carpooling

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.Icon
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.SwitchCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.Overlay
import org.osmdroid.views.overlay.Polyline
import java.io.*
import java.util.*


private const val REQUEST_CODE = 42
private const val FILE_NAME = "photo.jpg"
private lateinit var photoFile: File


class TripEditFragment : Fragment() {

    private lateinit var editDescription: EditText
    private lateinit var editArrival: EditText
    private lateinit var editNumberSeat: EditText
    private lateinit var editDuration: EditText
    private lateinit var editPrice: EditText
    private lateinit var editDeparture: EditText
    private lateinit var editDate: DatePicker
    private lateinit var editTime: TimePicker
    private lateinit var disableBooking: Button


    private var year = -1
    private var month = -1
    private var dayOfMonth = -1
    private var canBeBooked: Boolean? = null
    private var hours = -1
    private var minutes = -1

    private var modifyImg = 0
    private lateinit var editCarPhoto: ImageButton
    private val IMAGE_REQUEST_CODE = 100

    private var trip = defaultTrip()

    private var db = FirebaseFirestore.getInstance()
    private val tripsCollection = db.collection("trips")


    private lateinit var interestedUserView: RecyclerView
    private lateinit var interestedUserLabel: TextView
    var adapter: InterestedUserAdapter? = null
    private val interestedUserList = arrayListOf<User>()

    val acceptedUsers = arrayListOf<String>()
    val removedUsers = arrayListOf<String>()

    private lateinit var map: CustomMapView;
    private lateinit var buttonEditDepartureMarker: Button
    private lateinit var buttonEditArrivalMarker: Button
    private lateinit var buttonGoUp: Button
    private lateinit var mainScrollView: ScrollView
    private lateinit var myIcon: Icon
    private var departureMarker: Marker? = null
    private var arrivalMarker: Marker? = null
    private var editMapMod = EditMapMode.NONE

    private var arrivalGeo: GeoPoint? = null
    private var departureGeo: GeoPoint? = null

    private lateinit var polyline: Polyline

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hideFloatingButton()
        if (savedInstanceState != null) {
            modifyImg = savedInstanceState.getInt("modifyImg")
            year = savedInstanceState.getInt("YEAR")
            month = savedInstanceState.getInt("MONTH")
            dayOfMonth = savedInstanceState.getInt("DAY")
            hours = savedInstanceState.getInt("HOURS")
            minutes = savedInstanceState.getInt("MINUTES")
            canBeBooked = savedInstanceState.getBoolean("bookableTrip")
            savedInstanceState.getStringArrayList("acceptedUsers")?.let { acceptedUsers.addAll(it) }
            savedInstanceState.getStringArrayList("removedUsers")?.let { removedUsers.addAll(it) }

            val debug = savedInstanceState.getDouble("arrivalLat")

            if (savedInstanceState.getDouble("arrivalLat") != 0.0) {
                arrivalGeo = GeoPoint(
                    savedInstanceState.getDouble("arrivalLat"),
                    savedInstanceState.getDouble("arrivalLon")
                )
                departureGeo = GeoPoint(
                    savedInstanceState.getDouble("departureLat"),
                    savedInstanceState.getDouble("departureLon")
                )
            }

        }

    }

    private fun hideFloatingButton() {
        val test = activity?.findViewById<FloatingActionButton>(R.id.fab)
        if (test != null) {
            test.visibility = View.GONE
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hideFloatingButton()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        setHasOptionsMenu(true);
        val id = arguments?.getString("tripId")

        var view = inflater.inflate(R.layout.fragment_trip_edit, container, false)

        val orientation = this.resources.configuration.orientation


        if (orientation != Configuration.ORIENTATION_PORTRAIT) {
            view = inflater.inflate(R.layout.fragment_trip_edit_landscape, container, false)
        }

        startView(view)

        startInterestedUserView(view)
        buttonMapInit(view)
        mapInit(view)

        if (id != null) {
            readSharedPreferenceTrip(id)
        } else {
            trip = defaultTrip()
            updateEditLayout(modifyImg, year)
            disableBooking.visibility = View.GONE
            view.findViewById<Button>(R.id.endButton).visibility = View.GONE
        }



        if (arrivalGeo != null) {
            removeMarker(map, "Arrival")
            createArrivalMarker(map, arrivalGeo as GeoPoint)
        }

        if (departureGeo != null) {
            removeMarker(map, "Departure")
            createDepartureMarker(map, departureGeo as GeoPoint)
        }

        map.overlays.add(object : Overlay() {

            override fun onSingleTapConfirmed(
                e: MotionEvent,
                mapView: MapView
            ): Boolean {
                val projection =
                    mapView.projection
                val geoPoint = projection.fromPixels(
                    e.x.toInt(),
                    e.y.toInt()
                )


                when (editMapMod) {
                    EditMapMode.NONE -> {

                    }
                    EditMapMode.ARRIVAL -> {
                        removeMarker(map, "Arrival")
                        createArrivalMarker(
                            mapView,
                            GeoPoint(geoPoint.latitude, geoPoint.longitude)
                        )
                    }
                    EditMapMode.DEPARTURE -> {
                        removeMarker(map, "Departure")
                        createDepartureMarker(
                            mapView,
                            GeoPoint(geoPoint.latitude, geoPoint.longitude)
                        )

                    }
                }
                return true
            }
        })


        val endButton = view.findViewById<Button>(R.id.endButton)
        endButton.setOnClickListener {
            endTrip()
            val bundle = bundleOf("tripId" to trip.id)
            requireView().findNavController()
                .navigate(R.id.action_nav_editTrip_to_nav_rateAcceptedUsers, bundle)
        }


        return view
    }

    private fun startView(view: View) {

        editDescription = view.findViewById<EditText>(R.id.descriptionShowEdit)
        editArrival = view.findViewById<EditText>(R.id.arrivalShowEdit)
        editDeparture = view.findViewById<EditText>(R.id.departureShowEdit)
        editDate = view.findViewById<DatePicker>(R.id.DateShow)
        editDate.minDate = System.currentTimeMillis() - 1000
        editTime = view.findViewById<TimePicker>(R.id.TimeShow)
        editPrice = view.findViewById<EditText>(R.id.priceShowEdit)
        editNumberSeat = view.findViewById<EditText>(R.id.numberOfAvailableShowEdit)
        editDuration = view.findViewById<EditText>(R.id.tripDurationShowEdit)


        val buttonEditImage = view.findViewById<ImageButton>(R.id.carPhotoEdit)
        buttonEditImage.setOnClickListener { it ->
            it.performLongClick()
        }
        registerForContextMenu(buttonEditImage)
        editCarPhoto = buttonEditImage

        disableBooking = view.findViewById<Button>(R.id.disableButton)

        disableBooking.setOnClickListener {
            if (trip.canBeBooked) {
                setBookedProp(false)
                disableBooking.text = "Enable Booking"
            } else {
                setBookedProp(true)
                disableBooking.text = "Disable Booking"
            }
        }


    }

    private fun defaultTrip(): Trip {
        val d = Date()
        val cal = Calendar.getInstance()
        cal.time = d
        val day = cal.timeInMillis
        return Trip(
            "new", "Departure Location",
            "Arrival Location",
            day,
            10, 4, 10, "Description"
        )
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        super.onCreateOptionsMenu(menu, inflater)

        val item = menu.findItem(R.id.action_settings)
        val edit = menu.findItem(R.id.edit)
        val save = menu.findItem(R.id.save)
        item.isVisible = false
        edit.isVisible = false
        save.isVisible = false

        inflater.inflate(R.menu.edit_menu, menu)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.save -> {
                getValuesFromEditLayout()
                var directionToHome = true
                if (trip.id != "new")
                    directionToHome = false
                writeSharedPreferenceTrip()
                updateAcceptedUsersList()
                if (directionToHome)
                    requireView().findNavController().navigate(R.id.action_nav_editTrip_to_nav_home)
                else {
                    val bundle = bundleOf("tripId" to trip.id)
                    bundle.putInt("fromList", 1)
                    requireView().findNavController()
                        .navigate(R.id.action_nav_editTrip_to_nav_showTrip, bundle)
                }
            }
        }
        return true
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {

        super.onCreateContextMenu(menu, v, menuInfo)
        menu.setHeaderTitle("Choose your option")
        activity?.menuInflater?.inflate(R.menu.context_menu, menu)

    }

    override fun onContextItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.option_gallery -> {
                selectGallery()
                true
            }
            R.id.option_picture -> {
                takePicture()
                true
            }
            else -> super.onContextItemSelected(item)
        }

    }

    private fun takePicture() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        photoFile = getPhotoFile(FILE_NAME)

        val fileProvider =
            activity?.let {
                FileProvider.getUriForFile(
                    it,
                    "edu.standford.rkpandey.fileprovider",
                    photoFile
                )
            }

        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider)

        startActivityForResult(takePictureIntent, REQUEST_CODE)
    }

    private fun getPhotoFile(fileName: String): File {

        val storageDirectory = activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)

        return File.createTempFile(fileName, ".jpg", storageDirectory)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        val myImageView = editCarPhoto

        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            val takenImage = BitmapFactory.decodeFile(photoFile.absolutePath)

            val ei = ExifInterface(photoFile.absolutePath)
            val orientation: Int = ei.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )

            var rotatedBitmap: Bitmap? = null

            rotatedBitmap = when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(
                    takenImage, 90
                )
                ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(takenImage, 180)
                ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(takenImage, 270)
                ExifInterface.ORIENTATION_NORMAL -> takenImage
                else -> takenImage
            }


            myImageView.setImageBitmap(rotatedBitmap)
            if (rotatedBitmap != null) {
                storeTmpImg(rotatedBitmap)
            }

        } else if (requestCode == IMAGE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            myImageView.setImageURI(data?.data)
            val bitmap = (myImageView.drawable as BitmapDrawable).bitmap
            storeTmpImg(bitmap)

        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }

    }

    private fun selectGallery() {

        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_REQUEST_CODE)

    }


    private fun writeSharedPreferenceTrip() {


        val userEmail = FirebaseAuth.getInstance().currentUser?.email


        if (userEmail != null) {
            trip.userEmail = userEmail
            if (trip.id == "new")
                tripsCollection.add(trip).addOnSuccessListener { documentReference ->
                    Log.d(
                        "TEST DOCUMENT ID",
                        "DocumentSnapshot written with ID: ${documentReference.id}"
                    )
                    if (modifyImg != 0) {
                        val bitmap = (editCarPhoto.drawable as BitmapDrawable).bitmap
                        saveImage(bitmap, documentReference.id)
                    }
                }
            else {
                tripsCollection.document(trip.id).update(trip.TripToMutableMap())
                    .addOnSuccessListener {
                        if (modifyImg != 0) {
                            val bitmap = (editCarPhoto.drawable as BitmapDrawable).bitmap
                            saveImage(bitmap, trip.id)
                        }
                    }

            }
        }


    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun readSharedPreferenceTrip(id: String) {

        tripsCollection.document(id).get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    trip.arrivalLocation = document["arrivalLocation"].toString()
                    trip.availableSeats = (document["availableSeats"] as Long).toInt()
                    trip.departureDate = document["departureDate"] as Long
                    trip.departureLocation = document["departureLocation"].toString()
                    trip.description = document["description"].toString()
                    trip.duration = (document["duration"] as Long).toInt()
                    trip.id = id
                    trip.price = (document["price"] as Long).toInt()
                    trip.userEmail = document["userEmail"].toString()

                    if (document["canBeBooked"] == null) {
                        trip.canBeBooked = true
                    } else {
                        trip.canBeBooked = document["canBeBooked"] as Boolean
                    }



                    interestedUserList.clear()
                    if (document["interestedUsers"] != null) {
                        val listTmp = document["interestedUsers"] as ArrayList<*>
                        for (e in listTmp) {
                            interestedUserList.add(
                                User("FullName", "NickName", e.toString(), "Location")
                            )
                            adapter?.notifyDataSetChanged()
                        }
                    }

                    if (document["arrivalGeo"] != null && arrivalGeo == null) {
                        val arrivalMap = document["arrivalGeo"] as Map<*, *>
                        trip.arrivalGeo.latitude = (arrivalMap["latitude"] as Double)
                        trip.arrivalGeo.longitude = (arrivalMap["longitude"] as Double)
                    }
                    if (document["departureGeo"] != null && departureGeo == null) {
                        val departureMap = document["departureGeo"] as Map<*, *>
                        trip.departureGeo.latitude = (departureMap["latitude"] as Double)
                        trip.departureGeo.longitude = (departureMap["longitude"] as Double)
                    }

                }
                updateEditLayout(modifyImg, year)
            }.addOnFailureListener {
                updateEditLayout(modifyImg, year)
            }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getValuesFromEditLayout() {

        trip.description = editDescription.text.toString()
        trip.duration = editDuration.text.toString().toInt()
        trip.departureLocation = editDeparture.text.toString()
        trip.arrivalLocation = editArrival.text.toString()
        trip.availableSeats = editNumberSeat.text.toString().toInt()
        trip.price = editPrice.text.toString().toInt()


        val cal = Calendar.getInstance()
        cal[Calendar.DAY_OF_MONTH] = editDate.dayOfMonth
        cal[Calendar.MONTH] = editDate.month
        cal[Calendar.YEAR] = editDate.year
        cal[Calendar.HOUR] = editTime.hour
        cal[Calendar.MINUTE] = editTime.minute

        trip.departureDate = cal.timeInMillis


    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun updateEditLayout(modImg: Int, year: Int) {

        if (year == -1) {

            editDescription.setText(trip.description, TextView.BufferType.EDITABLE)
            editArrival.setText(trip.arrivalLocation, TextView.BufferType.EDITABLE)
            editDeparture.setText(trip.departureLocation, TextView.BufferType.EDITABLE)
            editPrice.setText(trip.price.toString(), TextView.BufferType.EDITABLE)
            editNumberSeat.setText(trip.availableSeats.toString(), TextView.BufferType.EDITABLE)
            editDuration.setText(trip.duration.toString(), TextView.BufferType.EDITABLE)

            val calendar = Calendar.getInstance()
            calendar.timeInMillis = trip.departureDate
            editDate.updateDate(
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            )
            editTime.hour = calendar.get(Calendar.HOUR)
            editTime.minute = calendar.get(Calendar.MINUTE)

            if (trip.arrivalGeo.longitude != 0.0) {
                removeMarker(map, "Arrival")
                createArrivalMarker(
                    map,
                    GeoPoint(trip.arrivalGeo.latitude, trip.arrivalGeo.longitude)
                )

            }
            if (trip.departureGeo.longitude != 0.0) {
                removeMarker(map, "Departure")
                createDepartureMarker(
                    map,
                    GeoPoint(trip.departureGeo.latitude, trip.departureGeo.longitude)
                )
            }


        } else {
            editDate.updateDate(
                year, month, dayOfMonth
            )
            editTime.hour = hours
            editTime.minute = minutes
        }


        if (modImg == 0) {
            val b = loadImage(activity?.filesDir?.path + "/../app_images")
            if (b != null) {
                editCarPhoto.setImageBitmap(b)
            }
        } else {
            val myBitmap = BitmapFactory.decodeFile(activity?.cacheDir?.path + "/tempBMP.jpg")
            if (myBitmap != null) {
                editCarPhoto.setImageBitmap(myBitmap)
            }
        }

        if (canBeBooked != null) {
            trip.canBeBooked = canBeBooked as Boolean
            canBeBooked = null

        }

        if (trip.canBeBooked) {
            disableBooking.text = "Disable Booking"
        } else {
            disableBooking.text = "Enable Booking"
        }


    }


    private fun rotateImage(source: Bitmap, angle: Int): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle.toFloat())
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }

    private fun saveImage(img: Bitmap, id: String): Uri {

        val wrapper = ContextWrapper(activity?.applicationContext)
        var file = wrapper.getDir("images", Context.MODE_PRIVATE)
        file = File(file, "carPhotoId$id.jpg")

        try {
            val stream: OutputStream = FileOutputStream(file)
            img.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException) { // Catch the exception
            e.printStackTrace()
        }
        return Uri.parse(file.absolutePath)

    }

    private fun storeTmpImg(myBitmap: Bitmap) {

        try {
            val outStream = FileOutputStream(File(activity?.cacheDir, "tempBMP.jpg"))
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 75, outStream)
            outStream.flush()
            outStream.close();
            modifyImg = 1
        } catch (e: IOException) {
            e.printStackTrace()
        }


    }

    override fun onSaveInstanceState(outState: Bundle) {

        super.onSaveInstanceState(outState)
        outState.putInt("modifyImg", modifyImg)
        outState.putBoolean("bookableTrip", trip.canBeBooked)
        outState.putStringArrayList("acceptedUsers", acceptedUsers)
        outState.putStringArrayList("removedUsers", removedUsers)
        outState.putDouble("arrivalLat", trip.arrivalGeo.latitude)
        outState.putDouble("arrivalLon", trip.arrivalGeo.longitude)
        outState.putDouble("departureLat", trip.departureGeo.latitude)
        outState.putDouble("departureLon", trip.departureGeo.longitude)

        if (this::editDate.isInitialized) {
            outState.putInt("YEAR", editDate.year)
            outState.putInt("MONTH", editDate.month)
            outState.putInt("DAY", editDate.dayOfMonth)
            outState.putInt("HOURS", editTime.currentHour)
            outState.putInt("MINUTES", editTime.currentMinute)
        } else {
            outState.putInt("YEAR", -1)
            outState.putInt("MONTH", -1)
            outState.putInt("DAY", -1)
            outState.putInt("HOURS", -1)
            outState.putInt("MINUTES", -1)
        }


    }

    private fun loadImage(path: String): Bitmap? {

        var b: Bitmap? = null
        try {
            val f = File(path, "carPhotoId" + trip.id + ".jpg")
            b = BitmapFactory.decodeStream(FileInputStream(f))
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        return b

    }

    private fun startInterestedUserView(view: View) {
        interestedUserLabel = view.findViewById<TextView>(R.id.listOfInterestedUserLabel)
        interestedUserView = view.findViewById<RecyclerView>(R.id.listOfInterestedUser)
        interestedUserView.layoutManager
        adapter = InterestedUserAdapter(interestedUserList, this)
        interestedUserView.adapter = adapter

    }

    private fun addAcceptedUser(userEmail: String): Boolean {
        val n = editNumberSeat.text.toString().toInt()
        return if (n >= 1) {
            acceptedUsers.add(userEmail)
            removedUsers.removeAll { it -> it == userEmail }
            editNumberSeat.setText((n - 1).toString(), TextView.BufferType.EDITABLE)
            true
        } else
            false

    }


    private fun updateAcceptedUsersList() {
        for (email in acceptedUsers)
            tripsCollection.document(trip.id)
                .update("acceptedUsers", FieldValue.arrayUnion(email))
        for (email in removedUsers)
            tripsCollection.document(trip.id)
                .update("acceptedUsers", FieldValue.arrayRemove(email))
    }

    private fun removeAcceptedUser(userEmail: String) {
        val n = editNumberSeat.text.toString().toInt()
        removedUsers.add(userEmail)
        acceptedUsers.removeAll { it -> it == userEmail }
        editNumberSeat.setText((n + 1).toString(), TextView.BufferType.EDITABLE)

    }

    private fun updateFireStoreAvailableSeats() {
        tripsCollection.document(trip.id)
            .update("availableSeats", trip.availableSeats)
    }


    fun checkIfUserIsAccepted(
        userEmail: String,
        switchCompat: SwitchCompat
    ) {
        tripsCollection.document(trip.id).get().addOnSuccessListener { document ->
            if (document["acceptedUsers"] != null) {
                val listTmp = document["acceptedUsers"] as ArrayList<*>
                if (userEmail in listTmp && acceptedUsers.isEmpty() && removedUsers.isEmpty()) {
                    switchCompat.isChecked = true
                } else if (userEmail in acceptedUsers)
                    switchCompat.isChecked = true
                else if (userEmail in removedUsers)
                    switchCompat.isChecked = false

                if (switchCompat.isChecked)
                    switchCompat.text = "Remove"

            }

            switchCompat.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    // The switch enable

                    val success = addAcceptedUser(userEmail)
                    if (success)
                        switchCompat.text = "Remove"
                    else
                        switchCompat.isChecked = false

                } else {
                    // The switch disable
                    switchCompat.text = "Accept"
                    removeAcceptedUser(userEmail)

                }
            }

        }

    }


    private fun setBookedProp(value: Boolean) {
        trip.canBeBooked = value
    }

    private fun removeMarker(map: MapView, id: String) {
        map.overlays.forEach {
            if (it is Marker && it.id == id) {
                map.overlays.remove(it)
            }
        }
    }


    private fun createArrivalMarker(mapView: MapView, geoPoint: GeoPoint) {
        arrivalMarker = Marker(mapView)
        arrivalMarker!!.id = "Arrival"
        arrivalMarker!!.position = GeoPoint(geoPoint.latitude, geoPoint.longitude);
        arrivalMarker!!.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
        mapView.overlays.add(arrivalMarker)

        arrivalMarker!!.icon =
            context?.let { ContextCompat.getDrawable(it, R.drawable.ic_destination_marker) }

        trip.arrivalGeo.latitude = geoPoint.latitude
        trip.arrivalGeo.longitude = geoPoint.longitude
        updatePolyLine()
    }


    private fun createDepartureMarker(mapView: MapView, geoPoint: GeoPoint) {
        departureMarker = Marker(mapView)
        departureMarker!!.id = "Departure"
        departureMarker!!.position =
            GeoPoint(geoPoint.latitude, geoPoint.longitude)
        departureMarker!!.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)

        departureMarker!!.icon = context?.let { ContextCompat.getDrawable(it, R.drawable.person) }

        mapView.overlays.add(departureMarker)
        trip.departureGeo.latitude = geoPoint.latitude
        trip.departureGeo.longitude = geoPoint.longitude
        updatePolyLine()
    }

    private fun updatePolyLine() {
        if (trip.departureGeo.latitude != 0.0 && trip.arrivalGeo.latitude != 0.0) {
            val pathPoints = arrayListOf<GeoPoint>(
                GeoPoint(trip.departureGeo.latitude, trip.departureGeo.longitude),
                GeoPoint(trip.arrivalGeo.latitude, trip.arrivalGeo.longitude)
            )
            polyline.setPoints(pathPoints)
        }
    }

    private fun buttonMapInit(view: View) {
        buttonEditArrivalMarker = view.findViewById(R.id.buttonEditArr)
        buttonEditArrivalMarker.setOnClickListener { it ->
            when (editMapMod) {
                EditMapMode.NONE -> {
                    buttonEditDepartureMarker.visibility = View.INVISIBLE
                    buttonEditArrivalMarker.text = "Confirm Arrival"
                    editMapMod = EditMapMode.ARRIVAL
                }
                EditMapMode.ARRIVAL -> {
                    buttonEditDepartureMarker.visibility = View.VISIBLE
                    buttonEditArrivalMarker.text = "Edit Arrival"
                    editMapMod = EditMapMode.NONE
                }
                EditMapMode.DEPARTURE -> {
                    buttonEditDepartureMarker.visibility = View.INVISIBLE
                    buttonEditArrivalMarker.text = "Confirm Arrival"
                    editMapMod = EditMapMode.ARRIVAL
                }
            }
        }

        buttonEditDepartureMarker = view.findViewById(R.id.buttonEditDep)
        buttonEditDepartureMarker.setOnClickListener { it ->
            when (editMapMod) {
                EditMapMode.NONE -> {
                    buttonEditArrivalMarker.visibility = View.INVISIBLE
                    buttonEditDepartureMarker.text = "Confirm Departure"
                    editMapMod = EditMapMode.DEPARTURE
                }
                EditMapMode.ARRIVAL -> {
                    buttonEditArrivalMarker.visibility = View.INVISIBLE
                    buttonEditDepartureMarker.text = "Confirm Departure"
                    editMapMod = EditMapMode.DEPARTURE
                }
                EditMapMode.DEPARTURE -> {

                    buttonEditArrivalMarker.visibility = View.VISIBLE

                    buttonEditDepartureMarker.text = "Edit Departure"
                    editMapMod = EditMapMode.NONE
                }
            }
        }
        buttonGoUp = view.findViewById(R.id.UpButton)
        buttonGoUp.setOnClickListener { it ->
            mainScrollView = view.findViewById(R.id.scrollView);
            mainScrollView.fullScroll(ScrollView.FOCUS_UP);
        }
    }


    private fun mapInit(view: View) {
        (activity as MainActivity).map = view.findViewById(R.id.mapEdit) as CustomMapView
        map = (activity as MainActivity).map
        map.setTileSource(TileSourceFactory.MAPNIK);
        val mapController = (activity as MainActivity).map.controller
        mapController.setZoom(7.0)
        val startPoint = GeoPoint(41.8719, 12.5674);
        mapController.setCenter(startPoint);
        polyline = Polyline()
        polyline.color = Color.RED
        polyline.width = 10F
        map.overlays.add(polyline);
    }


    private fun endTrip() {
        tripsCollection.document(trip.id).update("completed", true)
        tripsCollection.document(trip.id).update("canBeBooked", false)
    }

    companion object {

    }
}