package it.polito.carpooling

import android.app.Activity
import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.Rating
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import java.io.*


private const val FILE_NAME = "photo.jpg"

class ShowProfileActivity : Fragment() {

    private val user = User()

    private lateinit var fullName: TextView
    private lateinit var nickname: TextView
    private lateinit var email: TextView
    private lateinit var location: TextView
    private lateinit var photo: ImageView

    var db = FirebaseFirestore.getInstance()
    val usersProfile = db.collection("users")


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)


    }

    private fun hideFloatingButton() {
        val test = activity?.findViewById<FloatingActionButton>(R.id.fab)
        if (test != null) {
            test.visibility = View.GONE
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hideFloatingButton()
    }

    override fun onResume() {

        super.onResume()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)


        val orientation = this.resources.configuration.orientation
        var view = inflater.inflate(R.layout.activity_show_profile, container, false)

        if (orientation != Configuration.ORIENTATION_PORTRAIT) {
            view = inflater.inflate(R.layout.activity_show_profile_land, container, false)
        }
        fullName = view.findViewById<TextView>(R.id.showFullName)
        nickname = view.findViewById<TextView>(R.id.showNickname)
        email = view.findViewById<TextView>(R.id.showEmail)
        location = view.findViewById<TextView>(R.id.showLocation)
        photo = view.findViewById<ImageView>(R.id.imageView3)

        readSharedPreferenceUserInfo()

        val otherUserEmail = arguments?.getString("userEmail")

        val factory = object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return if (otherUserEmail != null)
                    UserReviewsViewModel(otherUserEmail) as T
                else {
                    val userEmail = FirebaseAuth.getInstance().currentUser?.email
                    userEmail?.let { UserReviewsViewModel(it) } as T
                }

            }
        }

        val viewModel: UserReviewsViewModel by lazy {
            ViewModelProvider(this, factory).get(UserReviewsViewModel::class.java)
        }

        viewModel.reviews.observe(viewLifecycleOwner, Observer {
            var averagePass = 0.0
            var averageDriver = 0.0
            var sumPass = 0.0
            var numberPass = 0
            var sumDriver = 0.0
            var numberDrive = 0
            for (i in it.indices) {

                if (it[i].type == "Driver") {
                    sumDriver += it[i].rating
                    numberDrive++
                } else {
                    sumPass += it[i].rating
                    numberPass++
                }
            }
            if (numberPass != 0)
                averagePass = sumPass / numberPass
            if (numberDrive != 0)
                averageDriver = sumDriver / numberDrive

            val ratingPassenger = view.findViewById<RatingBar>(R.id.passengerRating)
            ratingPassenger.rating = averagePass.toFloat()

            val ratingDriver = view.findViewById<RatingBar>(R.id.driverRating)
            ratingDriver.rating = averageDriver.toFloat()

        })


        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        super.onCreateOptionsMenu(menu, inflater)

        val item = menu.findItem(R.id.action_settings)
        val edit = menu.findItem(R.id.edit)
        val save = menu.findItem(R.id.save)
        if (item != null) item.isVisible = false
        edit.isVisible = false
        save.isVisible = false
        val otherUserEmail = arguments?.getString("userEmail")
        if (otherUserEmail == null) {
            inflater.inflate(R.menu.menu, menu)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.edit -> {
                editProfile()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)

        if (Activity.RESULT_OK == resultCode) {
        }

    }


    private fun editProfile() {

        val bundle = Bundle()

        //val intent = Intent(this@ShowProfileActivity, EditProfileActivity::class.java)
        bundle.putString("it.polito.carpooling.FULLNAME", user.fullName)
        bundle.putString("it.polito.carpooling.NICKNAME", user.nickName)
        bundle.putString("it.polito.carpooling.EMAIL", user.email)
        bundle.putString("it.polito.carpooling.LOCATION", user.location)


        val snackBar = Snackbar.make(
            requireActivity().findViewById(android.R.id.content),
            "Modify user info", Snackbar.LENGTH_SHORT
        )
        snackBar.show()

        requireView().findNavController()
            .navigate(R.id.action_nav_showProfile_to_nav_editProfile, bundle)

    }

    private fun readSharedPreferenceUserInfo() {
        val res = arrayListOf<String>()
        var userEmail = FirebaseAuth.getInstance().currentUser?.email
        val otherUserEmail = arguments?.getString("userEmail")
        if (otherUserEmail != null) {
            userEmail = otherUserEmail
        }
        if (userEmail != null) {
            usersProfile.document(userEmail).get()
                .addOnSuccessListener { document ->
                    if (document != null && document["fullName"] != null) {
                        res.add(document["fullName"].toString())
                        res.add(document["nickName"].toString())
                        res.add(document["email"].toString())
                        res.add(document["location"].toString())
                        user.fullName = res[0]
                        user.nickName = res[1]
                        user.email = res[2]
                        user.location = res[3]

                    }
                    updateShowLayout()
                }
                .addOnFailureListener { exception ->
                }
        }

    }


    private fun updateShowLayout() {

        fullName.text = user.fullName
        nickname.text = user.nickName
        email.text = user.email
        location.text = user.location

        val otherUserEmail = arguments?.getString("userEmail")
        val b = loadImage(this.activity?.filesDir?.path + "/../app_images")
        if (b != null && otherUserEmail == null) {
            photo.setImageBitmap(b)
        }

    }


    private fun loadImage(path: String): Bitmap? {

        var b: Bitmap? = null
        try {
            val f = File(path, FILE_NAME)
            b = BitmapFactory.decodeStream(FileInputStream(f))
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        return b

    }
}


