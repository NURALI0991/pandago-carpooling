package it.polito.carpooling

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent


class CustomMapView(context: Context?, attrs: AttributeSet) :
    org.osmdroid.views.MapView(context, attrs) {


    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        /**
         * Request all parents to relinquish the touch events
         */
        parent.requestDisallowInterceptTouchEvent(true)
        return super.dispatchTouchEvent(ev)
    }


}
