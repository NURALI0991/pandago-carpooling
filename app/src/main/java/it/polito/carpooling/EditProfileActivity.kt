package it.polito.carpooling

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.*
import android.view.ContextMenu.ContextMenuInfo
import android.widget.*
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.io.*


private const val FILE_NAME = "photo.jpg"
private const val REQUEST_CODE = 42
private lateinit var photoFile: File


class EditProfileActivity : Fragment() {

    private val user = User()

    private var modifyImg = 0

    private lateinit var editFullName: EditText
    private lateinit var editNickname: EditText
    private lateinit var editEmail: EditText
    private lateinit var editLocation: EditText
    private lateinit var editPhoto: ImageView

    var db = FirebaseFirestore.getInstance()
    val usersProfile = db.collection("users")
    val userProfile = usersProfile.document("O10XSOe8HwQiJNpVkSkN")

    val IMAGE_REQUEST_CODE = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        if (savedInstanceState != null) {
            modifyImg = savedInstanceState.getInt("modifyImg")
        }

        if (arguments?.isEmpty == false) {
            user.fullName = arguments?.getString("it.polito.carpooling.FULLNAME").toString()
            user.nickName = arguments?.getString("it.polito.carpooling.NICKNAME").toString()
            user.email = arguments?.getString("it.polito.carpooling.EMAIL").toString()
            user.location = arguments?.getString("it.polito.carpooling.LOCATION").toString()
        }


        getIntentValues()

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hideFloatingButton()
    }

    private fun hideFloatingButton() {
        val test = activity?.findViewById<FloatingActionButton>(R.id.fab)
        if (test != null) {
            test.visibility = View.GONE
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        setHasOptionsMenu(true)
        val orientation = this.resources.configuration.orientation
        var view = inflater.inflate(R.layout.activity_edit_profile, container, false)

        if (orientation != Configuration.ORIENTATION_PORTRAIT) {
            view = inflater.inflate(R.layout.activity_edit_profile_land, container, false)
        }

        editFullName = view.findViewById<EditText>(R.id.editFullName)
        editNickname = view.findViewById<EditText>(R.id.editNickname)
        editEmail = view.findViewById<EditText>(R.id.editEmail)
        editLocation = view.findViewById<EditText>(R.id.editLocation)
        editPhoto = view.findViewById<ImageView>(R.id.imageView5)
        updateEditLayout(modifyImg)

        val buttonEditImage = view.findViewById<Button>(R.id.editImage)
        buttonEditImage.setOnClickListener { it ->
            it.performLongClick()
        }
        registerForContextMenu(buttonEditImage)


        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        super.onCreateOptionsMenu(menu, inflater)

        val item = menu.findItem(R.id.action_settings)
        val edit = menu.findItem(R.id.edit)
        val save = menu.findItem(R.id.save)
        if (item != null) item.isVisible = false
        edit.isVisible = false
        save.isVisible = false

        inflater.inflate(R.menu.edit_menu, menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.save -> {
                getValuesFromEditLayout()
                saveData()
                (activity as MainActivity).updateThumbProfile()
                requireView().findNavController()
                    .navigate(R.id.action_nav_editProfile_to_nav_showProfile)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }


    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo?) {

        super.onCreateContextMenu(menu, v, menuInfo)
        menu.setHeaderTitle("Choose your option")
        activity?.menuInflater?.inflate(R.menu.context_menu, menu)

    }

    override fun onContextItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.option_gallery -> {
                selectGallery()
                true
            }
            R.id.option_picture -> {
                takePicture()
                true
            }
            else -> super.onContextItemSelected(item)
        }

    }


    private fun saveData() {

        val userEmail =  FirebaseAuth.getInstance().currentUser?.email

        if (userEmail != null) {
            usersProfile.document(userEmail).set(user)
        }

        val bitmap = (editPhoto.drawable as BitmapDrawable).bitmap
        saveImage(bitmap)


    }

    private fun takePicture() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        photoFile = getPhotoFile(FILE_NAME)

        val fileProvider =
            activity?.let {
                FileProvider.getUriForFile(
                    it,
                    "edu.standford.rkpandey.fileprovider",
                    photoFile
                )
            }

        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider)

        startActivityForResult(takePictureIntent, REQUEST_CODE)
    }

    private fun getPhotoFile(fileName: String): File {

        val storageDirectory = activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)

        return File.createTempFile(fileName, ".jpg", storageDirectory)

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        val myImageView = editPhoto

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {

            val takenImage = BitmapFactory.decodeFile(photoFile.absolutePath)

            val ei = ExifInterface(photoFile.absolutePath)
            val orientation: Int = ei.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )

            var rotatedBitmap: Bitmap? = null

            rotatedBitmap = when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(
                    takenImage, 90
                )
                ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(takenImage, 180)
                ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(takenImage, 270)
                ExifInterface.ORIENTATION_NORMAL -> takenImage
                else -> takenImage
            }


            myImageView.setImageBitmap(rotatedBitmap)
            if (rotatedBitmap != null) {
                storeTmpImg(rotatedBitmap)
            }

        } else if (requestCode == IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            myImageView.setImageURI(data?.data)
            val bitmap = (myImageView.drawable as BitmapDrawable).bitmap
            storeTmpImg(bitmap)

        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }

    }

    private fun selectGallery() {

        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_REQUEST_CODE)

    }


    private fun getIntentValues() {

        user.fullName = arguments?.getString("it.polito.carpooling.FULLNAME").toString()
        user.nickName = arguments?.getString("it.polito.carpooling.NICKNAME").toString()
        user.email = arguments?.getString("it.polito.carpooling.EMAIL").toString()
        user.location = arguments?.getString("it.polito.carpooling.LOCATION").toString()


    }

    private fun updateEditLayout(modImg: Int) {


        editFullName.setText(user.fullName, TextView.BufferType.EDITABLE)
        editNickname.setText(user.nickName, TextView.BufferType.EDITABLE)
        editEmail.setText(user.email, TextView.BufferType.EDITABLE)
        editLocation.setText(user.location, TextView.BufferType.EDITABLE)




        if (modImg == 0) {
            val b = loadImage(activity?.filesDir?.path + "/../app_images")
            if (b != null) {
                editPhoto.setImageBitmap(b)
            }
        } else {
            val myBitmap = BitmapFactory.decodeFile(activity?.cacheDir?.path + "/tempBMP.jpg")
            if (myBitmap != null) {
                editPhoto.setImageBitmap(myBitmap)
            }
        }

    }

    private fun getValuesFromEditLayout() {
        user.fullName = editFullName.text.toString()
        user.nickName = editNickname.text.toString()
        user.email = editEmail.text.toString()
        user.location = editLocation.text.toString()

    }

    private fun saveImage(img: Bitmap): Uri {

        val wrapper = ContextWrapper(activity?.applicationContext)
        var file = wrapper.getDir("images", Context.MODE_PRIVATE)
        file = File(file, FILE_NAME)

        try {
            val stream: OutputStream = FileOutputStream(file)
            img.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException) { // Catch the exception
            e.printStackTrace()
        }
        return Uri.parse(file.absolutePath)

    }

    private fun loadImage(path: String): Bitmap? {

        var b: Bitmap? = null
        try {
            val f = File(path, FILE_NAME)
            b = BitmapFactory.decodeStream(FileInputStream(f))
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        return b

    }


    private fun storeTmpImg(myBitmap: Bitmap) {

        try {
            val outStream = FileOutputStream(File(activity?.cacheDir, "tempBMP.jpg"))
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 75, outStream)
            outStream.flush()
            outStream.close();
            modifyImg = 1
        } catch (e: IOException) {
            e.printStackTrace()
        }


    }

    override fun onSaveInstanceState(outState: Bundle) {

        super.onSaveInstanceState(outState)
        outState.putInt("modifyImg", modifyImg)

    }


    private fun rotateImage(source: Bitmap, angle: Int): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle.toFloat())
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }


}
