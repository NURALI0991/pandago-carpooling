package it.polito.carpooling

import org.osmdroid.util.GeoPoint

class Trip(
    var id: String,
    var departureLocation: String, var arrivalLocation: String,
    var departureDate: Long, var duration: Int,
    var availableSeats: Int,
    var price: Int,
    var description: String
) {

    var userEmail = ""
    var canBeBooked = true

    var departureGeo = GeoPoint(0.0, 0.0)
    var arrivalGeo = GeoPoint(0.0, 0.0)


    fun TripToArray(): Array<String> {
        return arrayOf<String>(id.toString(), description)
    }

    fun TripToMutableMap(): MutableMap<String, Any> {
        val map = mutableMapOf<String, Any>()

        map["id"] = id
        map["departureLocation"] = departureLocation
        map["arrivalLocation"] = arrivalLocation
        map["departureDate"] = departureDate
        map["availableSeats"] = availableSeats
        map["price"] = price
        map["description"] = description
        map["duration"] = duration
        map["canBeBooked"] = canBeBooked

        map["arrivalGeo"] = arrivalGeo
        map["departureGeo"] = departureGeo

        return map
    }
}
