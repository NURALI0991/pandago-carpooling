package it.polito.carpooling

import android.accessibilityservice.GestureDescription
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.res.Configuration
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RatingBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import it.polito.carpooling.UserReview.Companion.toUserReview
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import java.lang.Exception
import java.util.ArrayList


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class RateAcceptedUsers : Fragment() {

    private var db = FirebaseFirestore.getInstance()
    private val tripsCollection = db.collection("trips")

    private var test: MutableMap<String, User>? = null
    private var listOfEmail = arrayListOf<String>()

    private lateinit var ratingBar: RatingBar
    private lateinit var reviewDescription: EditText
    private lateinit var userName: TextView
    private lateinit var nickName: TextView

    private var reviewType = "Passenger"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        var view = inflater.inflate(R.layout.fragment_rate_accepted_users, container, false)

        val orientation = this.resources.configuration.orientation


        if (orientation != Configuration.ORIENTATION_PORTRAIT) {
            view = inflater.inflate(R.layout.fragment_rate_accepted_users_land, container, false)
        }

        val sendButton = view.findViewById<Button>(R.id.sendButton)
        sendButton.setOnClickListener { it ->
            if (listOfEmail.isNotEmpty()) {
                val userEmail = listOfEmail.elementAt(0)
                val user = test?.get(userEmail)
                val review = user?.let { it1 -> getReview(it1, userEmail) }
                listOfEmail.removeAt(0)

                if (review != null) {
                    FireBaseUserReviewService.rateAUser(review)
                }
                if (listOfEmail.isEmpty()) {
                    requireView().findNavController()
                        .navigate(R.id.action_nav_rateAcceptedUsers_to_nav_home)
                } else {
                    val tmp = listOfEmail.elementAt(0)
                    test?.get(tmp)?.let { it1 -> initializeRateView(it1) }
                }

            }
        }

        ratingBar = view.findViewById(R.id.userRating)
        reviewDescription = view.findViewById(R.id.descriptionReviewEdit)
        userName = view.findViewById(R.id.showFullName)
        nickName = view.findViewById(R.id.showNickname)

        val id = arguments?.getString("tripId")

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var id = arguments?.getString("tripId")
        if (id == null) {
            id = ""
        }
        viewLifecycleOwner.lifecycleScope.launch {
            test = getAcceptedUsers(id)
            var i = 0
            if (test != null) {
                for ((key, value) in test!!) {
                    if (i == 0)
                        initializeRateView(value)
                    listOfEmail.add(key)
                    i++
                }
            } else {
                requireView().findNavController()
                    .navigate(R.id.action_nav_rateAcceptedUsers_to_nav_home)
            }

        }
    }

    private suspend fun getAcceptedUsers(tripId: String): MutableMap<String, User> {
        val usersMap = mutableMapOf<String, User>()

        val tripDocument = try {
            tripsCollection.document(tripId).get().await()
        } catch (e: Exception) {
            null
        }
        var acceptedUsersList: ArrayList<*>? = null

        val driver = arguments?.getBoolean("DRIVER")
        if (driver == true) {
            reviewType = "Driver"
        }

        if (tripDocument != null) {
            if (tripDocument["acceptedUsers"] != null && (driver == false || driver == null)) {
                acceptedUsersList = tripDocument["acceptedUsers"] as ArrayList<*>
            } else if (driver == true) {
                acceptedUsersList = arrayListOf(tripDocument["userEmail"])
            }
        }

        if (acceptedUsersList != null) {
            for (acceptedUser in acceptedUsersList) {
                val user = try {
                    val userDocument =
                        db.collection("users").document(acceptedUser.toString()).get().await()
                    if (userDocument.exists()) {
                        User(
                            userDocument["fullName"].toString(),
                            userDocument["nickName"].toString(),
                            userDocument["email"].toString(),
                            userDocument["location"].toString()
                        )
                    } else {
                        throw Exception("No user")
                    }
                } catch (e: Exception) {
                    User()
                }
                usersMap[acceptedUser.toString()] = user
            }
        }

        if (acceptedUsersList == null || acceptedUsersList.isEmpty()) {
            requireView().findNavController()
                .navigate(R.id.action_nav_rateAcceptedUsers_to_nav_home)
        }

        return usersMap
    }

    private fun initializeRateView(user: User) {
        ratingBar.rating = 0f
        reviewDescription.text.clear()
        userName.text = user.fullName
        nickName.text = user.nickName
    }


    private fun getReview(user: User, userEmail: String): UserReview {
        val reviewBy = FirebaseAuth.getInstance().currentUser?.email
        return UserReview(
            "new",
            userEmail,
            reviewBy.toString(),
            reviewType,
            ratingBar.rating.toDouble(),
            reviewDescription.text.toString()
        )
    }

    companion object {
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RateAcceptedUsers().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}