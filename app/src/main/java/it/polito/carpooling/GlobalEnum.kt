package it.polito.carpooling

enum class OtherTripNav {
    OTHERS, INTERESTED, BOUGHT
}

enum class EditMapMode { ARRIVAL, DEPARTURE, NONE, ADD_INTERMEDIATE, REMOVE_INTERMEDIATE }