package it.polito.carpooling

import android.annotation.SuppressLint
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import com.google.firebase.firestore.DocumentSnapshot
import java.lang.Exception

@SuppressLint("ParcelCreator")
data class UserReview(
    val reviewId: String,
    val userEmail: String,
    val reviewBy: String,
    val type: String,
    val rating: Double,
    val description: String
) : Parcelable {
    companion object {
        fun DocumentSnapshot.toUserReview(): UserReview? {
            return try {
                val value = getDouble("rating")!!
                val userEmail = getString("userEmail")!!
                val reviewBy = getString("reviewBy")!!
                val type = getString("type")!!
                val description = getString("description")!!
                UserReview(id, userEmail, reviewBy, type, value, description)
            } catch (e: Exception) {
                Log.e(TAG, "Error converting user review")
                null
            }
        }

        private const val TAG = "UserReview"
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
    }

}
