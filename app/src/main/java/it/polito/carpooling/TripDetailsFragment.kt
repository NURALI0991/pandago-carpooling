package it.polito.carpooling

import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.ScrollView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import org.json.JSONObject
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.Polyline
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.util.*


class TripDetailsFragment : Fragment() {

    private var trip = defaultTrip()
    private lateinit var description: TextView
    private lateinit var carPhoto: ImageView
    private lateinit var nAvailable: TextView
    private lateinit var departure: TextView
    private lateinit var arrival: TextView
    private lateinit var price: TextView
    private lateinit var duration: TextView
    private lateinit var timeD: TextView
    private lateinit var dateT: TextView

    private lateinit var rateDriver: Button

    private lateinit var interestedUserView: RecyclerView
    private lateinit var interestedUserLabel: TextView
    var adapter: InterestedUserAdapter? = null
    private val interestedUserList = arrayListOf<User>()

    private var ownerFlag = true
    private lateinit var mainScrollView: ScrollView
    private lateinit var map: CustomMapView;
    private lateinit var polyline: Polyline

    private var db = FirebaseFirestore.getInstance()
    private val tripsCollection = db.collection("trips")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hideFloatingButton()
    }

    private fun hideFloatingButton() {
        val test = activity?.findViewById<FloatingActionButton>(R.id.fab)
        if (test != null) {
            test.visibility = View.GONE
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true);

        var view = inflater.inflate(R.layout.fragment_trip_details, container, false)

        val orientation = this.resources.configuration.orientation


        if (orientation != Configuration.ORIENTATION_PORTRAIT) {
            view = inflater.inflate(R.layout.fragment_trip_details_landscape, container, false)
        }

        startView(view)

        hideFloatingButton()

        val button = view.findViewById<Button>(R.id.buttonIntermediateStop)
        button.setOnClickListener {
            val bundle = bundleOf("tripId" to trip.id)
            bundle.putBoolean("fromList", ownerFlag)
            val snackBar = Snackbar.make(
                requireActivity().findViewById(android.R.id.content),
                "Intermediate stops", Snackbar.LENGTH_SHORT
            )
            snackBar.show()
            requireView().findNavController()
                .navigate(R.id.action_nav_showTrip_to_nav_IntermediateStop, bundle)
        }

        val buttonGoUp = view.findViewById<Button>(R.id.UpButton)
        buttonGoUp.setOnClickListener {
            mainScrollView = view.findViewById(R.id.scrollView);
            mainScrollView.fullScroll(ScrollView.FOCUS_UP);
        }


        val id = arguments?.getString("tripId")
        val flagFrom = arguments?.getInt("fromList")

        if (flagFrom != 1) {
            ownerFlag = false
        }


        startInterestedUserView(view)
        startBookView(view)


        if (id != null) {
            readSharedPreferenceTrip(id)
        }

        mapInit(view)

        rateDriver = view.findViewById(R.id.buttonRateDriver)
        rateDriver.setOnClickListener {
            val bundle = bundleOf("DRIVER" to true)
            bundle.putString("tripId", trip.id)
            requireView().findNavController()
                .navigate(R.id.action_nav_showTrip_to_nav_rateAcceptedUsers, bundle)
        }

        return view

    }


    private fun startInterestedUserView(view: View) {
        interestedUserLabel = view.findViewById<TextView>(R.id.listOfInterestedUserLabel)
        interestedUserView = view.findViewById<RecyclerView>(R.id.listOfInterestedUser)

        if (!ownerFlag) {
            interestedUserView.visibility = View.GONE
            interestedUserLabel.visibility = View.GONE
        }
        interestedUserView.layoutManager
        adapter = InterestedUserAdapter(interestedUserList, this)
        interestedUserView.adapter = adapter

    }

    private fun startBookView(view: View) {
        if (!ownerFlag)
            bookATrip(view)
        else
            hideBookATrip(view)
    }

    private fun startView(view: View) {
        description = view.findViewById<TextView>(R.id.descriptionShow)
        carPhoto = view.findViewById<ImageView>(R.id.carPhoto)
        nAvailable = view.findViewById<TextView>(R.id.numberOfAvailableShow)
        arrival = view.findViewById<TextView>(R.id.arrivalShow)
        departure = view.findViewById<TextView>(R.id.departureShow)
        price = view.findViewById<TextView>(R.id.priceShow)
        duration = view.findViewById<TextView>(R.id.tripDurationShow)
        dateT = view.findViewById<TextView>(R.id.DateShow)
        timeD = view.findViewById<TextView>(R.id.TimeShow)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {

    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        val item = menu.findItem(R.id.action_settings)
        val edit = menu.findItem(R.id.edit)
        val save = menu.findItem(R.id.save)
        if (item != null) item.isVisible = false
        edit.isVisible = false
        save.isVisible = false
        if (ownerFlag)
            inflater.inflate(R.menu.menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.edit -> {
                val bundle = bundleOf("tripId" to trip.id)
                val snackBar = Snackbar.make(
                    requireActivity().findViewById(android.R.id.content),
                    "Modify trip details", Snackbar.LENGTH_SHORT
                )
                snackBar.show()
                requireView().findNavController()
                    .navigate(R.id.action_nav_showTrip_to_nav_editTrip, bundle)
            }
        }
        return true
    }


    private fun defaultTrip(): Trip {
        val d = Date()
        val cal = Calendar.getInstance()
        cal.time = d
        val day = cal.timeInMillis
        return Trip(
            "new", "Departure Location",
            "Arrival Location",
            day,
            10, 4, 10, "Description"
        )
    }

    private fun readSharedPreferenceTrip(id: String) {

        tripsCollection.document(id).get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    setTripDetailsValues(document, id)
                }
                if (trip.userEmail != FirebaseAuth.getInstance().currentUser?.email) {
                    ownerFlag = false
                    listenToDocument(id)
                }
                updateLayout()
            }.addOnFailureListener {

            }
    }


    private fun setTripDetailsValues(
        document: DocumentSnapshot,
        id: String,
        listenerFlag: Boolean = false
    ) {
        trip.arrivalLocation = document["arrivalLocation"].toString()
        trip.availableSeats = (document["availableSeats"] as Long).toInt()
        trip.departureDate = document["departureDate"] as Long
        trip.departureLocation = document["departureLocation"].toString()
        trip.description = document["description"].toString()
        trip.duration = (document["duration"] as Long).toInt()
        trip.id = id
        trip.price = (document["price"] as Long).toInt()
        trip.userEmail = document["userEmail"].toString()
        interestedUserList.clear()
        if (document["interestedUsers"] != null) {
            val listTmp = document["interestedUsers"] as ArrayList<*>
            for (e in listTmp) {
                interestedUserList.add(
                    User("FullName", "NickName", e.toString(), "Location")
                )
                adapter?.notifyDataSetChanged()
            }
        }

        if (document["arrivalGeo"] != null && document["departureGeo"] != null) {
            val arrivalMap = document["arrivalGeo"] as Map<*, *>
            val departureMap = document["departureGeo"] as Map<*, *>
            val geoPoint1 =
                GeoPoint(arrivalMap["latitude"] as Double, arrivalMap["longitude"] as Double)
            val geoPoint2 = GeoPoint(
                departureMap["latitude"] as Double,
                departureMap["longitude"] as Double
            )
            if (geoPoint1.latitude != 0.0 && geoPoint2.latitude != 0.0 && !listenerFlag) {
                createMarkersOnMap(map, geoPoint1, geoPoint2)
            }

        }

        val flagBought = arguments?.getInt("BOUGHT")
        if ((document["completed"] == true ) && flagBought != 0) {
            rateDriver.visibility = View.VISIBLE
        }


    }

    private fun updateLayout() {
        description.text = trip.description
        nAvailable.text = trip.availableSeats.toString()
        arrival.text = trip.arrivalLocation
        departure.text = trip.departureLocation
        price.text = trip.price.toString()
        duration.text = trip.duration.toString()

        val calendar = Calendar.getInstance()
        calendar.timeInMillis = trip.departureDate

        dateT.text = "" +
                numberPadding(calendar.get(Calendar.DAY_OF_MONTH)) + "-" + numberPadding(
            (calendar.get(
                Calendar.MONTH
            ) + 1)
        ) + "-" + calendar.get(
            Calendar.YEAR
        )
        timeD.text = timeString(calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE))

        val b = loadImage(this.activity?.filesDir?.path + "/../app_images")
        if (b != null) {
            carPhoto.setImageBitmap(b)
        }
        if (ownerFlag) {
            if (interestedUserList.isEmpty()) {
                interestedUserView.visibility = View.GONE
                interestedUserLabel.visibility = View.GONE
            } else {
                interestedUserView.visibility = View.VISIBLE
                interestedUserLabel.visibility = View.VISIBLE
            }
        }

    }


    private fun loadImage(path: String): Bitmap? {

        var b: Bitmap? = null
        try {
            val f = File(path, "carPhotoId" + trip.id + ".jpg")
            b = BitmapFactory.decodeStream(FileInputStream(f))
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        return b

    }

    private fun timeString(hour: Int, min: Int): String {
        var format = ""
        var hour = hour
        if (hour == 0) {
            hour += 12
            format = "AM"
        } else if (hour == 12) {
            format = "PM"
        } else if (hour > 12) {
            hour -= 12
            format = "PM"
        } else {
            format = "AM"
        }

        return "${numberPadding(hour)}:${numberPadding(min)} $format"
    }


    private fun listenToDocument(id: String) {
        val docRef = db.collection("trips").document(id)
        docRef.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            if (snapshot != null && snapshot.exists()) {
                Log.d(TAG, "Current data: ${snapshot.data}")
                setTripDetailsValues(snapshot, id, true)
                updateLayout()
            } else {
                Log.d(TAG, "Current data: null")
            }
        }
    }


    private fun bookATrip(view: View) {

        view.findViewById<FloatingActionButton>(R.id.bookButton)?.setOnClickListener {


            val userEmail = FirebaseAuth.getInstance().currentUser?.email

            tripsCollection.document(trip.id).update(
                "interestedUsers", FieldValue.arrayUnion(
                    userEmail
                )
            )
                .addOnSuccessListener {
                    val successedBook = Snackbar.make(
                        this.requireActivity().findViewById(android.R.id.content),
                        "Successful Ordered!", Snackbar.LENGTH_SHORT
                    )
                    successedBook.show()
                }
                .addOnFailureListener {
                    val unsuccessedBook = Snackbar.make(
                        this.requireActivity().findViewById(android.R.id.content),
                        "You have already ordered this trip!", Snackbar.LENGTH_SHORT
                    )
                    unsuccessedBook.show()
                }

        }
    }

    private fun hideBookATrip(view: View) {
        view.findViewById<FloatingActionButton>(R.id.bookButton)?.visibility = View.GONE
    }

    companion object {

        private const val TAG = "TRIP_DETAILS"

    }

    private fun numberPadding(x: Int): String {
        return if (x >= 10)
            x.toString()
        else
            "0$x"
    }

    fun checkIfUserIsAccepted(
        userEmail: String,
        tick: ImageView
    ) {
        tripsCollection.document(trip.id).get().addOnSuccessListener { document ->
            if (document["acceptedUsers"] != null) {
                val listTmp = document["acceptedUsers"] as ArrayList<*>
                if (userEmail in listTmp) {
                    tick.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun mapInit(view: View) {
        (activity as MainActivity).map = view.findViewById(R.id.mapShow) as CustomMapView
        map = (activity as MainActivity).map
        map.setTileSource(TileSourceFactory.MAPNIK);
        val mapController = (activity as MainActivity).map.controller
        mapController.setZoom(7.0)
        val startPoint = GeoPoint(41.8719, 12.5674);
        mapController.setCenter(startPoint);
        polyline = Polyline()
        polyline.color = Color.RED
        polyline.width = 10F
        map.overlays.add(polyline);
    }

    private fun createMarkersOnMap(mapView: MapView, geoPoint1: GeoPoint, geoPoint2: GeoPoint) {
        removeMarker(mapView, "Arrival")
        removeMarker(mapView, "Departure")
        val arrivalMarker = Marker(mapView)
        arrivalMarker.id = "Arrival"
        arrivalMarker.position = GeoPoint(geoPoint1.latitude, geoPoint1.longitude);
        arrivalMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
        arrivalMarker.icon =
            context?.let { ContextCompat.getDrawable(it, R.drawable.ic_destination_marker) }
        mapView.overlays.add(arrivalMarker)
        trip.arrivalGeo.latitude = geoPoint1.latitude
        trip.arrivalGeo.longitude = geoPoint1.longitude

        arrivalMarker.title = "Arrival"

        val departureMarker = Marker(mapView)
        departureMarker.id = "Departure"
        departureMarker.position =
            GeoPoint(geoPoint2.latitude, geoPoint2.longitude)
        departureMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
        departureMarker.icon = context?.let { ContextCompat.getDrawable(it, R.drawable.person) }

        departureMarker.title = "Departure"
        mapView.overlays.add(departureMarker)
        trip.departureGeo.latitude = geoPoint2.latitude
        trip.departureGeo.longitude = geoPoint2.longitude

        updatePolyLine()

    }

    private fun updatePolyLine() {
        if (trip.departureGeo.latitude != 0.0 && trip.arrivalGeo.latitude != 0.0) {
            val pathPoints = arrayListOf<GeoPoint>(
                GeoPoint(trip.departureGeo.latitude, trip.departureGeo.longitude),
                GeoPoint(trip.arrivalGeo.latitude, trip.arrivalGeo.longitude)
            )
            polyline.setPoints(pathPoints)
        }
    }

    private fun removeMarker(map: MapView, id: String) {
        map.overlays.forEach {
            if (it is Marker && it.id == id) {
                map.overlays.remove(it)
            }
        }
    }
}