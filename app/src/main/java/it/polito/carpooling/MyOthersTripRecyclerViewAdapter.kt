package it.polito.carpooling

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import it.polito.carpooling.dummy.DummyContent.DummyItem
import java.lang.Math.abs
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * [RecyclerView.Adapter] that can display a [DummyItem].
 * TODO: Replace the implementation with code for your data type.
 */
class MyOthersTripRecyclerViewAdapter(
    private val values: ArrayList<Trip>,
    private val navigateFrom: OtherTripNav
) :
    RecyclerView.Adapter<MyOthersTripRecyclerViewAdapter.ViewHolder>() {

    val valuesCopy = values.toMutableList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_trip_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.departure.text = item.departureLocation
        holder.arrival.text = item.arrivalLocation
        holder.nAvailableSeats.text = item.availableSeats.toString()
        holder.editButton.visibility = View.GONE
        holder.contentView.setOnClickListener { view ->
            val bundle = bundleOf("tripId" to item.id)
            bundle.putInt("fromList", 0)
            when (navigateFrom) {
                OtherTripNav.OTHERS -> {
                    view.findNavController().navigate(
                        R.id.action_nav_othersTripList_to_nav_showTrip,
                        bundle
                    )
                }
                OtherTripNav.INTERESTED -> {
                    view.findNavController().navigate(
                        R.id.action_nav_interestedTripList_to_nav_showTrip,
                        bundle
                    )
                }
                OtherTripNav.BOUGHT -> { // Note the block
                    bundle.putInt("BOUGHT", 1)
                    view.findNavController().navigate(
                        R.id.action_nav_boughtTripList_to_nav_showTrip,
                        bundle
                    )
                }
            }

        }
    }

    override fun getItemCount(): Int = values.size


    @RequiresApi(Build.VERSION_CODES.O)
    fun filter(
        arrivalLocation: String,
        departureLocation: String,
        price: String,
        dateString: String
    ): Boolean {
        values.clear()
        if (arrivalLocation.isEmpty() && departureLocation.isEmpty() && price.isEmpty() && dateString.isEmpty()) {
            values.addAll(valuesCopy)
        } else {

            var date = 0L
            val cal = Calendar.getInstance()
            if (isValidDate(dateString)) {
                val l = LocalDate.parse(dateString, DateTimeFormatter.ofPattern("dd-MM-yyyy"))
                cal[Calendar.DAY_OF_MONTH] = l.dayOfMonth
                cal[Calendar.MONTH] = l.monthValue - 1
                cal[Calendar.YEAR] = l.year
                date = cal.timeInMillis
            }


            val x = 0
            for (item in valuesCopy) {
                if (!((arrivalLocation.isNotEmpty() && !item.arrivalLocation.toLowerCase().contains(
                        arrivalLocation
                    )) ||
                            (departureLocation.isNotEmpty() && !item.departureLocation.toLowerCase()
                                .contains(
                                    departureLocation
                                )) ||
                            (price.isNotEmpty() && item.price > price.toInt()))
                ) {
                    val cal2 = Calendar.getInstance()
                    cal2.timeInMillis = item.departureDate
                    if (date == 0L || (date != 0L && sameDate(
                            cal,
                            cal2
                        ))
                    )
                        values.add(item)
                }
            }
        }
        notifyDataSetChanged()

        return values.isEmpty()
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val editButton: Button = view.findViewById(R.id.editRecycle)
        val contentView: LinearLayout = view.findViewById(R.id.restOfTheCard)
        val departure = view.findViewById<TextView>(R.id.cardDeparture)
        val arrival = view.findViewById<TextView>(R.id.cardArrival)
        val nAvailableSeats = view.findViewById<TextView>(R.id.cardNAvailableSits)
        override fun toString(): String {
            return "TEST"
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun isValidDate(inDate: String): Boolean {
        val dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy")
        try {
            LocalDate.parse(inDate, dateFormat)
        } catch (e: Exception) {
            return false
        }
        return true
    }

    private fun sameDate(cal: Calendar, cal2: Calendar): Boolean {
        return (cal[Calendar.DAY_OF_MONTH] == cal2[Calendar.DAY_OF_MONTH]
                && cal[Calendar.MONTH] == cal2[Calendar.MONTH]
                && cal[Calendar.YEAR] == cal2[Calendar.YEAR])
    }

}