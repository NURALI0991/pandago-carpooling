package it.polito.carpooling

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import it.polito.carpooling.dummy.DummyContent.DummyItem
import java.util.*
import kotlin.collections.ArrayList

class MyIntermediateStopRecyclerViewAdapter(
    private val values: ArrayList<IntermediateStop>, private val fragment: IntermediateStopFragment,
    private val flagOwner: Boolean
) : RecyclerView.Adapter<MyIntermediateStopRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_intermediate_stop, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]

        val calendar = Calendar.getInstance()
        calendar.timeInMillis = item.estimatedTime

        holder.idView.text = item.location
        holder.contentView.text = "" +
                numberPadding(calendar.get(Calendar.DAY_OF_MONTH)) + "-" + numberPadding(
            (calendar.get(
                Calendar.MONTH
            ) + 1)
        ) + "-" + calendar.get(
            Calendar.YEAR
        )


        holder.contentViewTime.text =
            " " + timeString(
                calendar.get(Calendar.HOUR),
                calendar.get(Calendar.MINUTE)
            )

        if (!flagOwner)
            holder.deleteButton.visibility = View.INVISIBLE

        holder.deleteButton.setOnClickListener {
            removeAt(position);
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val idView: TextView = view.findViewById(R.id.intermediateStopLocation)
        val contentView: TextView = view.findViewById(R.id.intermediateStopDate)

        val contentViewTime: TextView = view.findViewById(R.id.intermediateStopTime)

        val deleteButton: Button = view.findViewById(R.id.deleteIntermediate)

        override fun toString(): String {
            return super.toString() + " '" + contentView.text + "'"
        }
    }

    fun removeAt(position: Int) {
        val intermediateStopId = values[position].id
        fragment.deleteSharedPreferenceIntermediateStop(values[position]);
        values.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, values.size)

        if(itemCount==0){
            fragment.showNoIntermediateLayout()
        }

    }


    private fun timeString(hour: Int, min: Int): String {
        var format = ""
        var hour = hour
        if (hour == 0) {
            hour += 12
            format = "AM"
        } else if (hour == 12) {
            format = "PM"
        } else if (hour > 12) {
            hour -= 12
            format = "PM"
        } else {
            format = "AM"
        }

        return "${numberPadding(hour)}:${numberPadding(min)} $format"
    }

    private fun numberPadding(x: Int): String {
        return if (x >= 10)
            x.toString()
        else
            "0$x"
    }

}