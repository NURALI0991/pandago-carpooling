package it.polito.carpooling

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class BoughtTripsListFragment : Fragment() {

    private var columnCount = 1


    var adapter: MyOthersTripRecyclerViewAdapter? = null
    private lateinit var recView: RecyclerView
    private lateinit var empView: TextView
    private var db = FirebaseFirestore.getInstance()
    private val tripsCollection = db.collection("trips")

    private val tripsArray = arrayListOf<Trip>()

    private var arrivalLocationQuery = ""
    private var departureLocationQuery = ""
    private var priceQuery = ""
    private var dataQuery = ""


    private lateinit var searchView: SearchView
    private lateinit var empView2: TextView

    enum class filterType { ARRIVAL, DEPARTURE, DATA, PRICE }

    private lateinit var filterT: filterType


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        val view = inflater.inflate(R.layout.fragment_item_list, container, false)


        recView = view.findViewById<RecyclerView>(R.id.list)
        empView = view.findViewById<TextView>(R.id.empty_view)

        empView2 = view.findViewById<TextView>(R.id.empty_view2)



        hideFloatingButton()
        // Set the adapter
        tripsArray.clear()
        recView.layoutManager
        adapter = MyOthersTripRecyclerViewAdapter(tripsArray, OtherTripNav.BOUGHT)
        adapter?.valuesCopy?.clear()
        recView.adapter = adapter


        getOthersTrips()


        return view
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getSearchedTrips(recView: RecyclerView, empView: TextView) {
        if (adapter?.filter(
                arrivalLocationQuery,
                departureLocationQuery,
                priceQuery,
                dataQuery
            ) == true
        ) {
            showNoTripsLayout(recView, empView)
        } else {
            hideNoTripsLayout(recView, empView)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hideFloatingButton()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        val item = menu.findItem(R.id.action_settings)
        val edit = menu.findItem(R.id.edit)
        val save = menu.findItem(R.id.save)
        //if (item != null) item.isVisible = false
        edit.isVisible = false
        save.isVisible = false
        inflater.inflate(R.menu.others_trip, menu)
        searchView = menu.findItem(R.id.searchBar).actionView as SearchView
        initSearchView()


    }

    private fun initSearchView() {
        searchView.queryHint = "Search by Arrival location"
        filterT = filterType.ARRIVAL
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                searchView.clearFocus()
                return false
            }

            @RequiresApi(Build.VERSION_CODES.O)
            override fun onQueryTextChange(query: String): Boolean {

                when (filterT) {
                    filterType.ARRIVAL -> {
                        arrivalLocationQuery = query.toLowerCase()
                    }
                    filterType.DATA -> {
                        dataQuery = query.toLowerCase()
                    }
                    filterType.DEPARTURE -> {
                        departureLocationQuery = query.toLowerCase()
                    }
                    filterType.PRICE -> {
                        priceQuery = query.toLowerCase()
                    }
                }

                getSearchedTrips(recView, empView2)
                return false
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.serDep -> {
                searchView.queryHint = "Search by Departure location"
                searchView.inputType = InputType.TYPE_CLASS_TEXT
                filterT = filterType.DEPARTURE

            }
            R.id.serDate -> {
                searchView.queryHint = "Search by Date dd-mm-yyyy"
                searchView.inputType = InputType.TYPE_CLASS_DATETIME
                filterT = filterType.DATA
            }

            R.id.serPrice -> {
                searchView.queryHint = "Search by max Price"
                searchView.inputType = InputType.TYPE_CLASS_NUMBER
                filterT = filterType.PRICE
            }
            R.id.serArrival -> {
                searchView.queryHint = "Search by Arrival location"
                searchView.inputType = InputType.TYPE_CLASS_TEXT
                filterT = filterType.ARRIVAL
            }
            R.id.action_settings -> {
                (activity as MainActivity?)?.userLogOut()
            }
        }
        return true
    }

    private fun showNoTripsLayout(recView: RecyclerView, empView: TextView) {
        recView.visibility = View.GONE
        empView.visibility = View.VISIBLE
    }


    private fun hideNoTripsLayout(recView: RecyclerView, empView: TextView) {
        recView.visibility = View.VISIBLE
        empView.visibility = View.GONE
    }

    private fun hideFloatingButton() {
        val test = activity?.findViewById<FloatingActionButton>(R.id.fab)
        if (test != null) {
            test.visibility = View.GONE
        }
    }

    private fun getOthersTrips() {

        val userEmail = FirebaseAuth.getInstance().currentUser?.email

        if (userEmail != null) {
            tripsCollection.whereArrayContains("acceptedUsers", userEmail)
                .get()
                .addOnSuccessListener { documents ->
                    if (documents.size() == 0)
                        showNoTripsLayout(recView, empView)

                    for (document in documents) {
                        Log.d("TAG", "${document.id} => ${document.data}")
                        val trip = Trip(
                            document.id,
                            document["departureLocation"].toString(),
                            document["arrivalLocation"].toString(),
                            document["departureDate"] as Long,
                            (document["duration"] as Long).toInt(),
                            (document["availableSeats"] as Long).toInt(),
                            (document["price"] as Long).toInt(),
                            document["description"].toString()
                        )
                        trip.userEmail = document["userEmail"].toString()

                        if (document["canBeBooked"] != null) {
                            trip.canBeBooked = document["canBeBooked"] as Boolean
                        }

                        if (document["acceptedUsers"] != null) {
                            val listTmp = document["acceptedUsers"] as ArrayList<*>
                            if (userEmail in listTmp) {
                                tripsArray.add(trip)
                            }
                        }
                        adapter?.valuesCopy?.add(trip)
                        adapter?.notifyDataSetChanged()
                    }
                }
                .addOnFailureListener { exception ->
                    Log.w("TAG", "Error getting documents: ", exception)
                    showNoTripsLayout(recView, empView)
                }
        }

    }


    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            OthersTripFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}