package it.polito.carpooling


class User(var fullName: String = "Full Name", var nickName:String= "Nickname",
var email:String="email@example.com", var location: String = "Location") {

    fun userInfoToArray():Array<String>{
        return arrayOf<String>(fullName, nickName, email, location)
    }
}